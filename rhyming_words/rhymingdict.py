import os.path

from rhyming_words.list_interface import MyLinkedList
from rhyming_words.my_sorted_linklist import MySortedLinkedList
from rhyming_words.rhyme_group_words import RhymeGroupWords


def get_rhyme_group(line):
    first_space = line.index(" ")
    pronunciation = line[first_space + 1: len(line)]

    stress0 = pronunciation.find("0")
    stress1 = pronunciation.find("1")
    stress2 = pronunciation.find("2")

    if stress2 > 0:
        return pronunciation[stress2 - 2: len(pronunciation)]
    if stress1 > 0:
        return pronunciation[stress1 - 2: len(pronunciation)]
    if stress0 > 0:
        return pronunciation[stress0 - 2: len(pronunciation)]

    return pronunciation


def get_word(line):
    return line[0: line.index(" ")]


def load_dictionary():
    pwd = os.getcwd()
    path = f"{pwd}/cmudict/cmudict-short.dict"
    try:
        with open(path, 'r') as f:
            lines = f.readlines()
            lines = [line.strip("\r?\n") for line in lines]
        return lines
    except Exception as e:
        print(e)


if __name__ == '__main__':
    all_lines = load_dictionary()

    rhyme_groups = MyLinkedList()
    for lin in all_lines:
        rg = get_rhyme_group(lin)
        word = get_word(lin)
        for i in range(rhyme_groups.size()):
            list1 = rhyme_groups.get(i)
            if list1.value.get_rhyme_group() == rg:
                word_list = list1.value.get_word_list()
                word_list.add(word)
                break
        else:
            word_list = MySortedLinkedList()
            word_list.add(word)
            rgw = RhymeGroupWords(rg, word_list)
            rhyme_groups.add(rhyme_groups.size(), rgw)

    for i in range(rhyme_groups.size()):
        rg = rhyme_groups.get(i)
        print(f"{rg.value.get_rhyme_group()}: ")
        print(rg.value.get_word_list())
