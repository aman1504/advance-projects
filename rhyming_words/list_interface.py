
class Node:

    def __init__(self, value):
        self.next = None
        self.value = value


class MyLinkedList:

    def __init__(self):
        self.head = None
        self.length = 0

    def is_empty(self):
        return self.head is None

    def size(self):
        return self.length

    def add(self, index, value):
        try:
            if index > self.length:
                raise IndexError("Index is greater than size of linked list.")

            new_node = Node(value)
            if index == 0:
                new_node.next = self.head
                self.head = new_node
            else:
                current = self.head
                while index > 1:
                    if current is not None:
                        index -= 1
                        current = current.next
                new_node.next = current.next
                current.next = new_node
            self.length += 1
        except Exception as ex:
            print(ex)

    def remove(self, index):
        try:
            if index >= self.length:
                raise IndexError("Index is greater than size of linked list.")

            if index == 0:
                self.head = self.head.next
                return

            current = self.head
            while index > 1:
                if current:
                    current = current.next
                    index -= 1

            temp = current
            temp.next = current.next.next
        except Exception as e:
            print(e)

    def remove_all(self):
        self.head = None
        self.length = 0

    def get(self, index):
        try:
            if index > self.length:
                raise IndexError("Index is greater than size of linked list.")

            current = self.head
            while index > 0:
                if current is not None:
                    current = current.next
                    index -= 1
            return current
        except Exception as e:
            print(e)

        pass

    def find(self, node):
        if node is None:
            return -1

        current = self.head
        index = 0
        while current is not None:
            if current.value == node.value:
                return index
            index += 1
            current = current.next
        return -1

    def __str__(self):
        if self.is_empty():
            return "List is empty"
        s = "("
        current = self.head
        while current is not None:
            s += f"{current.value},"
            current = current.next

        s = s[0:len(s) - 1]
        s += ")"
        return s
