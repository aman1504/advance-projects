from rhyming_words.list_interface import MyLinkedList


class MySortedLinkedList(MyLinkedList):

    def add(self, value):
        if self.is_empty():
            super().add(0, value)
            return

        current = self.head
        index = 0
        while current:
            if current.value < value:
                current = current.next
                index += 1
            else:
                break

        super().add(index, value)
