
class RhymeGroupWords:

    def __init__(self, rg, wl):
        self.rhyme_group = rg
        self.word_list = wl

    def get_rhyme_group(self):
        return self.rhyme_group

    def get_word_list(self):
        return self.word_list
