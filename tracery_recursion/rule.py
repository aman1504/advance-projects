import random


class Rule:

    RANDOM = None

    def __init__(self, raw):
        self.RAW = raw
        self.SECTIONS = raw.split('#')

    @classmethod
    def set_seed(cls, seed):
        print(f"Set seed {seed}")
        cls.RANDOM = random.Random(seed)

    def expand(self, grammer):
        results = list()
        for count, val in enumerate(self.SECTIONS):
            if count % 2 == 0:
                results.append(val)
            else:
                word = grammer[val][self.RANDOM.randint(0, len(grammer[val]) - 1)]
                results.append(word.expand(grammer))
        return "".join(results)

    def __str__(self):
        return self.RAW

