import sys
import time

from tracery_recursion.rule import Rule


def load_grammer(gram_file):
    gram = dict()
    try:
        with open(gram_file, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            print(line)
            key, value = line.split(':')
            expansions = value.split(',')
            rules = list()
            for expansion in expansions:
                rule = Rule(expansion)
                rules.append(rule)

            gram[key] = rules

    except Exception as e:
        print(e)

    return gram


def output_grammer(gram):
    print("\nGRAMMAR:")
    for key, value in gram.items():
        line = f"{key}: "
        for rule in value:
            line += f"{rule},"

        print(line)


if __name__ == '__main__':
    print("Running TraceryRecursion...")
    grammer_file = 'grammar-story.txt'
    start_symbol = '#origin#'
    count = 1
    seed = (time.time() * 1000) % 1000

    args = sys.argv
    if len(args) == 5:
        grammer_file = args[1]
        start_symbol = args[2]
        count = args[3]
        seed = args[4]
    elif len(args) == 4:
        grammer_file = args[1]
        start_symbol = args[2]
        count = args[3]
    elif len(args) == 3:
        grammer_file = args[1]
        start_symbol = args[2]
    elif len(args) == 2:
        grammer_file = args[1]

    print(f"\twith grammer: {grammer_file}\t'startSymbol: {start_symbol}'\tcount: {count}\tseed:{seed}")
    Rule.set_seed(seed)
    grammer = load_grammer(grammer_file)
    output_grammer(grammer)

    rl = Rule(start_symbol)
    for i in range(int(count)):
        print(rl.expand(grammer))
